# frozen_string_literal: true

module Researchable
  module Messaging
    # Helper to send user related messages from SVC auth
    # @author Researchable
    class UserMessages
      USER_CONFIRMED_QUEUE = 'user_confirmed'
      USER_CREATED_QUEUE = 'user_created'
      MY_TOPIC = 'svc.auth'
      class << self
        def user_confirmed!(user)
          klass = Researchable::Protobuf::Messages::UserConfirmed
          msg = {
            uuid: user.uuid,
            email: user.email,
            created_at: user.created_at.to_i
          }
          enqueue(klass, msg, USER_CONFIRMED_QUEUE)
        end

        def user_created!(user)
          klass = Researchable::Protobuf::Messages::UserCreated
          msg = {
            uuid: user.uuid,
            email: user.email,
            created_at: user.created_at.to_i,
            terms_accepted_at: user.terms_accepted_at.to_i
          }
          queue = USER_CREATED_QUEUE
          enqueue(klass, msg, queue)
        end

        private

        def enqueue(klass, msg, queue)
          obj = klass.new(msg)
          message = klass.encode(obj)
          Researchable::RabbitConnection.enqueue(message, topic: MY_TOPIC, queue_name: queue)
        end
      end
    end
  end
end
