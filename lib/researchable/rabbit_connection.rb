# frozen_string_literal: true

module Researchable
  # Connection provider to send messages to the rabbit mq
  # @author Researchable
  class RabbitConnection
    include Singleton
    attr_reader :connection, :initialized

    def self.enqueue(message, topic: 'default', queue_name: 'default')
      return false unless instance.initialized?

      instance.channel.with do |channel|
        cur_exchange = instance.exchange(channel, topic)
        instance.setup_queue(channel, cur_exchange, queue_name)
        cur_exchange.publish(message, routing_key: queue_name)
      end
      true
    rescue StandardError => e
      # For now we gracefully fail if the enqueuing fails somewhere. This is
      # mostly here because we don't actually use MQs yet. 
      Rails.logger.warn(e.message)
    end

    def exchange(channel, topic)
      @exchange ||= channel.topic(topic)
    end

    def setup_queue(channel, exchange, queue_name)
      queue = channel.queue(queue_name)
      queue.bind(exchange, routing_key: queue_name)
    end

    def initialize
      cs = Rails.application.config.rabbit_mq_connection_string
      unless cs
        @initialized = false
        Rails.logger.info('Not initializing MQ')
        return
      end

      @initialized = true
      @connection = Bunny.new(cs)
      @connection.start
    rescue StandardError => e
      # For now we gracefully fail if the mq cannot connect for whatever reason
      Rails.logger.warn(e.message)
      @initialized = false
    end

    def initialized?
      @initialized
    end

    def channel
      return nil unless @initialized

      @channel ||= ConnectionPool.new do
        connection.create_channel
      end
    end
  end
end
